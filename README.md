# js-backend-template

Run MongoDB


### Template Link
This template started from the next link
https://www.callicoder.com/node-js-express-mongodb-restful-crud-api-tutorial/

### Run MongoDB
Before to start the app, run MongoDB from a terminal.

```sh
mongod --config /usr/local/etc/mongod.conf
```

### Run Project
Run in console

```sh
node server.js
```

### Test from Postman
Call to the endpoint localhost:3000/users with POST method.
In the body section, use type raw (JSON) with the next content:
{"name": "German", "last_name": "Dosko"}
