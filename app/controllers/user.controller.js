const User = require('../models/user.model.js');

// Create and Save a new User
exports.create = (req, res) => {
    // Validate request
    if(!req.body.name || !req.body.last_name) {
        return res.status(400).send({
            message: "Name and LastName can not be empty"
        });
    }

    console.log("req.body", req.body)
    // Create a user
    const user = new User({
        name: req.body.name,
        last_name: req.body.last_name
    });

    // Save user in the database
    console.log("Por crear usuario")
    user.save()
    .then(data => {
        console.log('funca');
        res.send(data);
    }).catch(err => {
        console.log('palmo');
        res.status(500).send({
            message: err.message || "Some error occurred while creating the User."
        });
    });
};

// Retrieve and return all users from the database.
exports.findAll = (req, res) => {

};

// Find a single user with a userId
exports.findOne = (req, res) => {

};

// Update a user identified by the userId in the request
exports.update = (req, res) => {

};

// Delete a user with the specified userId in the request
exports.delete = (req, res) => {

};
