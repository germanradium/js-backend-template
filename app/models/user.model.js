const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    name: String,
    last_name: String
}, {
    timestamps: true
});

module.exports = mongoose.model('User', UserSchema);
